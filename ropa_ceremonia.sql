CREATE DATABASE IF NOT EXISTS ropa_ceremonia_ud3;
USE ropa_ceremonia_ud3;
CREATE TABLE IF NOT EXISTS diseniador(
id int primary key auto_increment,
nombre varchar(20) not null,
apellidos varchar(40) not null,
fecha_nacimiento date,
genero varchar(20)
);
CREATE TABLE IF NOT EXISTS coleccion(
id int primary key auto_increment,
nombre varchar(50),
marca varchar(50),
estacion varchar(20),
fecha_creacion date,
id_diseniador int,
FOREIGN KEY (id_diseniador) REFERENCES diseniador(id) ON UPDATE cascade
);
CREATE TABLE IF NOT EXISTS prenda(
id int primary key auto_increment,
nombre varchar(50) unique,
tipo varchar(20),
color varchar(20),
talla int,
precio float,
fecha_compra date,
fecha_entrega date,
id_coleccion int,
FOREIGN KEY (id_coleccion) REFERENCES coleccion(id) ON UPDATE cascade
);

CREATE TABLE IF NOT EXISTS tienda(
id int primary key auto_increment,
nombre varchar(50),
direccion varchar(50),
plantas int
);

CREATE TABLE IF NOT EXISTS trabajador(
id int primary key auto_increment,
nombre varchar(30),
apellidos varchar(50),
fecha_nacimiento date,
cargo varchar(20),
id_tienda int,
FOREIGN KEY (id_tienda) references tienda(id) ON UPDATE cascade
);
CREATE TABLE IF NOT EXISTS coleccion_tienda(
idcoleccion int references coleccion,
idtienda int references tienda,
fecha_salida_en_tienda date,
primary key (idcoleccion, idtienda)
);