package com.lorenagr.ropa_ceremonia.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.lorenagr.ropa_ceremonia.enums.*;
import com.lorenagr.ropa_ceremonia.enums.Color;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame {
    private final static String TITULOFRAME = "Aplicación ropa de ceremonia hibernate";

    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JPanel panelDisen;
    private JPanel panelColeccion;
    private JPanel panelPrenda;
    private JPanel panelTienda;
    private JPanel panelTrabajador;

     JTextField txtNombreDisen;
     JTextField txtApellidosDisen;
     JComboBox cbGeneroDisen;
     JButton btnAniadirDisen;
     JButton btnModificarDisen;
     JButton btnEliminarDisen;
     JButton btnListarDisen;
     JList listaDiseniadores;
     JButton btnListarColecDisen;
     JList listaColeccionesDisen;
     DatePicker dateFechaNacDisen;

     JTextField txtNombreColeccion;
     JComboBox cbMarcaColeccion;
     JComboBox cbEstacionColeccion;
     JButton btnAniadirColeccion;
     JButton btnModificarColeccion;
     JButton btnEliminarColeccion;
     JButton btnListarColeccion;
     JButton btnListarPrendasColeccion;
     JList listaPrendasColeccion;
     JList listaColecciones;
     DatePicker dateFechaCreacionColeccion;
     JComboBox cbDiseniadorColeccion;


    JTextField txtNombrePrenda;
     JComboBox cbTipoPrenda;
     JComboBox cbColorPrenda;
     JSlider sliderTallaPrenda;
     JTextField txtPrecioPrenda;
    JButton btnAniadirPrenda;
     JButton btnModificarPrenda;
     JButton btnEliminarPrenda;
     JButton btnListarPrenda;
     JList listaPrendas;
     DatePicker dateFechaCompraPrenda;
     DatePicker dateFechaEntregaPrenda;
     JComboBox cbColeccionPrenda;


    JButton btnAniadirTienda;
     JButton btnModificarTienda;
     JButton btnEliminarTienda;
     JButton btnListarTienda;
     JButton btnListarTrabajadorTienda;
     JList listaTiendas;
     JList listaTrabajadoresTienda;
     JTextField txtNombreTienda;
     JTextField txtDireccionTienda;
     JSlider sliderPlantasTienda;

     JButton btnAniadirTrabajador;
     JButton btnModificarTrabajador;
     JButton btnEliminarTrabajador;
     JButton btnListarTrabajador;
     JList listaTrabajadores;
     JTextField txtNombreTrabajador;
     JTextField txtApellidosTrabajador;
    JComboBox cbCargoTrabajador;
     DatePicker dateFechaNacTrabajador;
     JComboBox cbTiendaTrabajador;

    // DEFAULT LIST MODEL
    DefaultListModel dlmColeccion;
    DefaultListModel dlmDiseniador;
    DefaultListModel dlmPrenda;
    DefaultListModel dlmTrabajador;
    DefaultListModel dlmTienda;
    DefaultListModel dlmColeccionDisen;
    DefaultListModel dlmPrendaColeccion;
    DefaultListModel dlmTrabajadorTienda;

    // MENU ITEMS
    JMenuItem conexionItem;
    JMenuItem salirItem;

    public Vista(){
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Metodo que hace la vista
     */
    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+500, this.getHeight()+100));
        this.setLocationRelativeTo(this);
        setMenu();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Metodo que crea el menu y añade la barra y los items
     */
     void setMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        conexionItem = new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");
        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");
        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        this.setJMenuBar(barra);
    }

    /**
     * Metodo que inicia y da modelo a los defaultlistmodel
     */
     void setTableModels() {
         dlmDiseniador = new DefaultListModel();
         listaDiseniadores.setModel(dlmDiseniador);
         dlmColeccion = new DefaultListModel();
         listaColecciones.setModel(dlmColeccion);
         dlmPrenda = new DefaultListModel();
         listaPrendas.setModel(dlmPrenda);
         dlmTienda = new DefaultListModel();
         listaTiendas.setModel(dlmTienda);
         dlmTrabajador = new DefaultListModel();
         listaTrabajadores.setModel(dlmTrabajador);
         dlmColeccionDisen = new DefaultListModel();
         listaColeccionesDisen.setModel(dlmColeccionDisen);
         dlmPrendaColeccion = new DefaultListModel();
         listaPrendasColeccion.setModel(dlmPrendaColeccion);
         dlmTrabajadorTienda = new DefaultListModel();
         listaTrabajadoresTienda.setModel(dlmTrabajadorTienda);
    }

    /**
     * Metodo que coge los elementos de un enum y los mete en un combobox
     */
     void setEnumComboBox(){
        for(com.lorenagr.ropa_ceremonia.enums.Color constant : Color.values()){
            cbColorPrenda.addItem(constant.getColor());
        }
        for(Estacion constant : Estacion.values()){
            cbEstacionColeccion.addItem(constant.getEstacion());
        }
        for(Marca constant : Marca.values()){
            cbMarcaColeccion.addItem(constant.getMarca());
        }
        for(Genero constant : Genero.values()){
            cbGeneroDisen.addItem(constant.getGenero());
        }
        for(Cargo constant : Cargo.values()){
            cbCargoTrabajador.addItem(constant.getCargo());
        }
        for(Tipo constant : Tipo.values()){
            cbTipoPrenda.addItem(constant.getTipo());
        }
    }
}

