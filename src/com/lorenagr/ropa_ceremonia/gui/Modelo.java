package com.lorenagr.ropa_ceremonia.gui;

import com.lorenagr.ropa_ceremonia.base.Coleccion;
import com.lorenagr.ropa_ceremonia.base.Prenda;
import com.lorenagr.ropa_ceremonia.base.Tienda;
import com.lorenagr.ropa_ceremonia.base.Diseniador;
import com.lorenagr.ropa_ceremonia.base.Trabajador;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

public class Modelo {

    SessionFactory sessionFactory;

    /**
     * Metodo que desconectar y cierra el sesion factory
     */
    public void desconectar(){
        if(sessionFactory != null && sessionFactory.isOpen()){
            sessionFactory.close();
        }
    }

    /**
     * Metodo que conectar, cogiendo la configuracion del archivo hibernate.cfg.xml
     * añade las clases del mapeo y crea coge las properties
     */
    public void conectar(){
        Configuration configuracion = new Configuration();

        configuracion.configure("hibernate.cfg.xml");
        configuracion.addAnnotatedClass(Coleccion.class);
        configuracion.addAnnotatedClass(Prenda.class);
        configuracion.addAnnotatedClass(Tienda.class);
        configuracion.addAnnotatedClass(Diseniador.class);
        configuracion.addAnnotatedClass(Trabajador.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    /**
     * Metodo que da de alta un diseñador
     * @param nuevoDiseniador
     */
    // DISEÑADOR
    public void altaDiseniador(Diseniador nuevoDiseniador){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoDiseniador);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que modifica un diseñador
     * @param disenSeleccionado
     */
    public void modificarDiseniador(Diseniador disenSeleccionado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(disenSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que elimina un diseñador
     * @param disenEliminado
     */
    public void eliminarDisen(Diseniador disenEliminado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(disenEliminado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo array que te permite coge el diseñador.
     * @return
     */
    public ArrayList<Diseniador> getDiseniador(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Diseniador");
        ArrayList<Diseniador> lista = (ArrayList<Diseniador>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo array que te deja coger las colecciones de un diseñador
     * @param coleccionDisen
     * @return
     */
    public ArrayList<Coleccion> getColeccionDisen(Diseniador coleccionDisen){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Coleccion WHERE id_diseniador =:prop");
        query.setParameter("prop", coleccionDisen.getId());
        ArrayList<Coleccion>lista = (ArrayList<Coleccion>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que da de alta una coleccion
     * @param nuevaColeccion
     */
    // COLECCION
    public void altaColeccion(Coleccion nuevaColeccion){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaColeccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que modifica una coleccion
     * @param coleccionSeleccionada
     */
    public void modificarColeccion(Coleccion coleccionSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(coleccionSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que elimina una coleccion
     * @param coleccionEliminada
     */
    public void eliminarColeccion(Coleccion coleccionEliminada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(coleccionEliminada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * metodo que te deja coger una coleccion
     * @return
     */
    public ArrayList<Coleccion> getColeccion(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Coleccion");
        ArrayList<Coleccion> listaColecciones = (ArrayList<Coleccion>)query.getResultList();
        sesion.close();
        return listaColecciones;
    }

    /**
     * Metodo array que coge una prenda de una coleccion
     * @param prendaColeccion
     * @return
     */
    public ArrayList<Prenda> getPrendaColeccion(Coleccion prendaColeccion){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Prenda WHERE id_coleccion =:prop");
        query.setParameter("prop", prendaColeccion.getId());
        ArrayList<Prenda>lista = (ArrayList<Prenda>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que da de alta prenda
     * @param nuevaPrenda
     */
    // PRENDA
    public void altaPrenda(Prenda nuevaPrenda){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaPrenda);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo modificar una prenda
     * @param prendaSeleccionada
     */
    public void modificarPrenda(Prenda prendaSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(prendaSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * METODO PARA ELIMINAR UNA PRENDA
     * @param prendaEliminada
     */
    public void eliminarPrenda(Prenda prendaEliminada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(prendaEliminada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * metodo que te coge una prenda
     * @return
     */
    public ArrayList<Prenda> getPrenda(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Prenda");
        ArrayList<Prenda> listaPrendas = (ArrayList<Prenda>)query.getResultList();
        sesion.close();
        return listaPrendas;
    }

    /**
     * Metodo que da de alta una tienda
     * @param nuevaTienda
     */
    // TIENDA
    public void altaTienda(Tienda nuevaTienda){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaTienda);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que modifica una tienda
     * @param tiendaSeleccionada
     */
    public void modificarTienda(Tienda tiendaSeleccionada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(tiendaSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que elimina una tienda
     * @param tiendaEliminada
     */
    public void eliminarTienda(Tienda tiendaEliminada){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(tiendaEliminada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que coge una tienda
     * @return
     */
    public ArrayList<Tienda> getTienda(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Tienda");
        ArrayList<Tienda> listaTienda = (ArrayList<Tienda>)query.getResultList();
        sesion.close();
        return listaTienda;
    }

    /**
     * Metodo que te coge un trabajador de una tienda
     * @param trabajadorTienda
     * @return
     */
    public ArrayList<Trabajador> getTiendaTrabajador(Tienda trabajadorTienda) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Trabajador WHERE id_tienda =:prop");
        query.setParameter("prop", trabajadorTienda.getId());
        ArrayList<Trabajador> lista = (ArrayList<Trabajador>) query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo de alta de trabajador
     * @param nuevoTrabajador
     */
    // TRABAJADOR
    public void altaTrabajador(Trabajador nuevoTrabajador){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoTrabajador);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que modifica un trabajador
     * @param trabajadorSeleccionado
     */
    public void modificarTrabajador(Trabajador trabajadorSeleccionado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(trabajadorSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que elimina un trabajador
     * @param trabajadorEliminado
     */
    public void eliminarTrabajador(Trabajador trabajadorEliminado){
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(trabajadorEliminado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que coge un trabajador
     * @return
     */
    public ArrayList<Trabajador> getTrabajador(){
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Trabajador");
        ArrayList<Trabajador> listTrabajadores = (ArrayList<Trabajador>)query.getResultList();
        sesion.close();
        return listTrabajadores;
    }
}
