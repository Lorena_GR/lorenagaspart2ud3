package com.lorenagr.ropa_ceremonia.gui;

import com.lorenagr.ropa_ceremonia.base.*;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;

public class Controlador implements ActionListener, ListSelectionListener {
    Vista vista;
    Modelo modelo;

    /**
     * Constructor que inicia la aplicacion activa los listlisteners y action listeners
     * y lista todos los campos de todas las tablas
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;

        modelo.conectar();
        listarTodo();
        addActionListeners(this);
        addListSelectionListeners(this);
    }

    /**
     * Metodo que añade los listlisteners a las listas de las tablas
     * @param listener
     */
    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listaDiseniadores.addListSelectionListener(listener);
        vista.listaColecciones.addListSelectionListener(listener);
        vista.listaPrendas.addListSelectionListener(listener);
        vista.listaTiendas.addListSelectionListener(listener);
        vista.listaTrabajadores.addListSelectionListener(listener);
    }

    /**
     * Metodo que añade los actionlisteners a los botones
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.conexionItem.addActionListener(listener);
        vista.salirItem.addActionListener(listener);
        vista.btnAniadirDisen.addActionListener(listener);
        vista.btnAniadirDisen.setActionCommand("AltaDisen");
        vista.btnAniadirColeccion.addActionListener(listener);
        vista.btnAniadirColeccion.setActionCommand("AltaColeccion");
        vista.btnAniadirPrenda.addActionListener(listener);
        vista.btnAniadirPrenda.setActionCommand("AltaPrenda");
        vista.btnAniadirTienda.addActionListener(listener);
        vista.btnAniadirTienda.setActionCommand("AltaTienda");
        vista.btnAniadirTrabajador.addActionListener(listener);
        vista.btnAniadirTrabajador.setActionCommand("AltaTrabajador");
        vista.btnModificarDisen.addActionListener(listener);
        vista.btnModificarDisen.setActionCommand("ModificarDisen");
        vista.btnModificarColeccion.addActionListener(listener);
        vista.btnModificarColeccion.setActionCommand("ModificarColeccion");
        vista.btnModificarPrenda.addActionListener(listener);
        vista.btnModificarPrenda.setActionCommand("ModificarPrenda");
        vista.btnModificarTienda.addActionListener(listener);
        vista.btnModificarTienda.setActionCommand("ModificarTienda");
        vista.btnModificarTrabajador.addActionListener(listener);
        vista.btnModificarTrabajador.setActionCommand("ModificarTrabajador");
        vista.btnEliminarDisen.addActionListener(listener);
        vista.btnEliminarDisen.setActionCommand("EliminarDisen");
        vista.btnEliminarColeccion.addActionListener(listener);
        vista.btnEliminarColeccion.setActionCommand("EliminarColeccion");
        vista.btnEliminarPrenda.addActionListener(listener);
        vista.btnEliminarPrenda.setActionCommand("EliminarPrenda");
        vista.btnEliminarTienda.addActionListener(listener);
        vista.btnEliminarTienda.setActionCommand("EliminarTienda");
        vista.btnEliminarTrabajador.addActionListener(listener);
        vista.btnEliminarTrabajador.setActionCommand("EliminarTrabajador");
        vista.btnListarDisen.addActionListener(listener);
        vista.btnListarDisen.setActionCommand("ListarDisen");
        vista.btnListarColeccion.addActionListener(listener);
        vista.btnListarColeccion.setActionCommand("ListarColeccion");
        vista.btnListarPrenda.addActionListener(listener);
        vista.btnListarPrenda.setActionCommand("ListarPrenda");
        vista.btnListarTienda.addActionListener(listener);
        vista.btnListarTienda.setActionCommand("ListarTienda");
        vista.btnListarTrabajador.addActionListener(listener);
        vista.btnListarTrabajador.setActionCommand("ListarTrabajador");
        vista.btnListarColecDisen.addActionListener(listener);
        vista.btnListarColecDisen.setActionCommand("ListarColeccionDisen");
        vista.btnListarPrendasColeccion.addActionListener(listener);
        vista.btnListarPrendasColeccion.setActionCommand("ListarPrendaColeccion");
        vista.btnListarTrabajadorTienda.addActionListener(listener);
        vista.btnListarTrabajadorTienda.setActionCommand("ListarTrabajadorTienda");
    }

    /**
     * Metodo que llama a todos los listar para usarlo al principio
     */
    public void listarTodo(){
        listarDiseniadores(modelo.getDiseniador());
        listarColecciones(modelo.getColeccion());
        listarPrendas(modelo.getPrenda());
        listarTiendas(modelo.getTienda());
        listarTrabajadores(modelo.getTrabajador());
    }

    /**
     * Metodo action performed que llevara el switch
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando){
            /**
             * Case que sale de la aplicación
             */
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            /**
             * Case que conecta
             */
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;
            /**
             * Case que da de alta a un diseñador
             */
            case "AltaDisen":
                Diseniador nuevoDisen = new Diseniador();
                nuevoDisen.setNombre(vista.txtNombreDisen.getText());
                nuevoDisen.setApellidos(vista.txtApellidosDisen.getText());
                nuevoDisen.setFechaNacimiento(Date.valueOf(vista.dateFechaNacDisen.getDate()));
                nuevoDisen.setGenero(String.valueOf(vista.cbGeneroDisen.getSelectedItem()));
                modelo.altaDiseniador(nuevoDisen);
                refrescarDiseniador();
                listarTodo();
                break;
            /**
             * Case que modifica un diseñador
             */
            case "ModificarDisen":
                Diseniador disenSeleccionado = (Diseniador)vista.listaDiseniadores.getSelectedValue();
                disenSeleccionado.setNombre(vista.txtNombreDisen.getText());
                disenSeleccionado.setApellidos(vista.txtApellidosDisen.getText());
                disenSeleccionado.setFechaNacimiento(Date.valueOf(vista.dateFechaNacDisen.getDate()));
                disenSeleccionado.setGenero(String.valueOf(vista.cbGeneroDisen.getSelectedItem()));
                modelo.modificarDiseniador(disenSeleccionado);
                refrescarDiseniador();
                listarTodo();
                break;
            /**
             * Case que elimina un diseñador
             */
            case "EliminarDisen":
                Diseniador disenBorrado = (Diseniador)vista.listaDiseniadores.getSelectedValue();
                modelo.eliminarDisen(disenBorrado);
                listarTodo();
                break;
            /**
             * Case que lista un diseñador
             */
            case "ListarDisen":
                listarDiseniadores(modelo.getDiseniador());
                break;
            /**
             * Case que lista las colecciones de un diseñador seleccionado
             */
            case "ListarColeccionDisen":
                Diseniador diseniadorNuevo = (Diseniador)vista.listaDiseniadores.getSelectedValue();
                diseniadorNuevo.setColecciones(modelo.getColeccionDisen(diseniadorNuevo));
                listarColeccionesDiseniador(modelo.getColeccionDisen(diseniadorNuevo));
                break;
            /**
             * Case que da de alta una coleccion
             */
            case "AltaColeccion":
                Coleccion nuevaColeccion = new Coleccion();
                nuevaColeccion.setNombre(vista.txtNombreColeccion.getText());
                nuevaColeccion.setMarca(String.valueOf(vista.cbMarcaColeccion.getSelectedItem()));
                nuevaColeccion.setEstacion(String.valueOf(vista.cbEstacionColeccion.getSelectedItem()));
                nuevaColeccion.setFechaCreacion(Date.valueOf(vista.dateFechaCreacionColeccion.getDate()));
                nuevaColeccion.setDiseniador((Diseniador)vista.cbDiseniadorColeccion.getSelectedItem());
                modelo.altaColeccion(nuevaColeccion);
                refrescarColeccion();
                listarTodo();
                break;
            /**
             * Case que modifica una coleccion
             */
            case "ModificarColeccion":
                Coleccion coleccionSeleccionada = (Coleccion)vista.listaColecciones.getSelectedValue();
                coleccionSeleccionada.setNombre(vista.txtNombreColeccion.getText());
                coleccionSeleccionada.setMarca(String.valueOf(vista.cbMarcaColeccion.getSelectedItem()));
                coleccionSeleccionada.setEstacion(String.valueOf(vista.cbEstacionColeccion.getSelectedItem()));
                coleccionSeleccionada.setFechaCreacion(Date.valueOf(vista.dateFechaCreacionColeccion.getDate()));
                coleccionSeleccionada.setDiseniador((Diseniador)vista.cbDiseniadorColeccion.getSelectedItem());
                modelo.modificarColeccion(coleccionSeleccionada);
                refrescarColeccion();
                listarTodo();
                break;
            /**
             * Case que elimina una coleccion
             */
            case "EliminarColeccion":
                Coleccion coleccionBorrada = (Coleccion)vista.listaColecciones.getSelectedValue();
                modelo.eliminarColeccion(coleccionBorrada);
                listarTodo();
                break;
            /**
             * Case que lista una coleccion
             */
            case "ListarColeccion":
                listarColecciones(modelo.getColeccion());
                break;
            /**
             * Case que lista las prendas de una coleccion
             */
            case "ListarPrendaColeccion":
                Coleccion coleccionNueva = (Coleccion) vista.listaColecciones.getSelectedValue();
                coleccionNueva.setPrendas(modelo.getPrendaColeccion(coleccionNueva));
                listarPrendasColeccion(modelo.getPrendaColeccion(coleccionNueva));
                break;
            /**
             * Case que da de alta una prenda
             */
            case "AltaPrenda":
                Prenda nuevaPrenda = new Prenda();
                nuevaPrenda.setNombre(vista.txtNombrePrenda.getText());
                nuevaPrenda.setTipo(String.valueOf(vista.cbTipoPrenda.getSelectedItem()));
                nuevaPrenda.setColor(String.valueOf(vista.cbColorPrenda.getSelectedItem()));
                nuevaPrenda.setTalla(vista.sliderTallaPrenda.getValue());
                nuevaPrenda.setPrecio(Double.parseDouble(vista.txtPrecioPrenda.getText()));
                nuevaPrenda.setFechaCompra(Date.valueOf(vista.dateFechaCompraPrenda.getDate()));
                nuevaPrenda.setFechaEntrega(Date.valueOf(vista.dateFechaEntregaPrenda.getDate()));
                nuevaPrenda.setColeccion((Coleccion) vista.cbColeccionPrenda.getSelectedItem());
                modelo.altaPrenda(nuevaPrenda);
                refrescarPrenda();
                listarPrendas(modelo.getPrenda());
                break;
            /**
             * Case que modifica una prenda
             */
            case "ModificarPrenda":
                Prenda prendaSeleccionada = (Prenda)vista.listaPrendas.getSelectedValue();
                prendaSeleccionada.setNombre(vista.txtNombrePrenda.getText());
                prendaSeleccionada.setTipo(String.valueOf(vista.cbTipoPrenda.getSelectedItem()));
                prendaSeleccionada.setColor(String.valueOf(vista.cbColorPrenda.getSelectedItem()));
                prendaSeleccionada.setTalla(vista.sliderTallaPrenda.getValue());
                prendaSeleccionada.setPrecio(Double.parseDouble(vista.txtPrecioPrenda.getText()));
                prendaSeleccionada.setFechaCompra(Date.valueOf(vista.dateFechaCompraPrenda.getDate()));
                prendaSeleccionada.setFechaEntrega(Date.valueOf(vista.dateFechaEntregaPrenda.getDate()));
                prendaSeleccionada.setColeccion((Coleccion) vista.cbColeccionPrenda.getSelectedItem());
                modelo.modificarPrenda(prendaSeleccionada);
                refrescarPrenda();
                listarPrendas(modelo.getPrenda());
                break;
            /**
             * Case que elimina una prenda
             */
            case "EliminarPrenda":
                Prenda prendaEliminada = (Prenda)vista.listaPrendas.getSelectedValue();
                modelo.eliminarPrenda(prendaEliminada);
                listarPrendas(modelo.getPrenda());
                break;
            /**
             * Case que lista una prenda
             */
            case "ListarPrenda":
                listarPrendas(modelo.getPrenda());
                break;
            /**
             * Case que da de alta una tienda
             */
            case "AltaTienda":
                Tienda nuevaTienda = new Tienda();
                nuevaTienda.setNombre(vista.txtNombreTienda.getText());
                nuevaTienda.setDireccion(vista.txtDireccionTienda.getText());
                nuevaTienda.setPlantas(vista.sliderPlantasTienda.getValue());
                modelo.altaTienda(nuevaTienda);
                refrescarTienda();
                listarTiendas(modelo.getTienda());
                break;
            /**
             * Case que modifica una tienda
             */
            case "ModificarTienda":
                Tienda tiendaSeleccionada = (Tienda)vista.listaTiendas.getSelectedValue();
                tiendaSeleccionada.setNombre(vista.txtNombreTienda.getText());
                tiendaSeleccionada.setDireccion(vista.txtDireccionTienda.getText());
                tiendaSeleccionada.setPlantas(vista.sliderPlantasTienda.getValue());
                modelo.modificarTienda(tiendaSeleccionada);
                refrescarTienda();
                listarTiendas(modelo.getTienda());
                break;
            /**
             * Case que elimina una tienda
             */
            case "EliminarTienda":
                Tienda tiendaEliminada = (Tienda)vista.listaTiendas.getSelectedValue();
                modelo.eliminarTienda(tiendaEliminada);
                listarTiendas(modelo.getTienda());
                break;
            /**
             * Case que lista una tienda
             */
            case "ListarTienda":
                listarTiendas(modelo.getTienda());
                break;
            /**
             * Case que lista los trabajadores de una tienda
             */
            case "ListarTrabajadorTienda":
                Tienda tiendaNueva = (Tienda) vista.listaTiendas.getSelectedValue();
                tiendaNueva.setTrabajadores(modelo.getTiendaTrabajador(tiendaNueva));
                listarTrabajadorTienda(modelo.getTiendaTrabajador(tiendaNueva));
                break;
            /**
             * Case que da de alta un trabajador
             */
            case "AltaTrabajador":
                Trabajador nuevoTrabajador = new Trabajador();
                nuevoTrabajador.setNombre(vista.txtNombreTrabajador.getText());
                nuevoTrabajador.setApellidos(vista.txtApellidosTrabajador.getText());
                nuevoTrabajador.setFechaNacimiento(Date.valueOf(vista.dateFechaNacTrabajador.getDate()));
                nuevoTrabajador.setCargo(String.valueOf(vista.cbCargoTrabajador.getSelectedItem()));
                nuevoTrabajador.setTienda((Tienda)vista.cbTiendaTrabajador.getSelectedItem());
                modelo.altaTrabajador(nuevoTrabajador);
                refrescarTrabajador();
                listarTrabajadores(modelo.getTrabajador());
                break;
            /**
             * Case que modifica un trabajador
             */
            case "ModificarTrabajador":
                Trabajador trabajadorSeleccionado = (Trabajador)vista.listaTrabajadores.getSelectedValue();
                trabajadorSeleccionado.setNombre(vista.txtNombreTrabajador.getText());
                trabajadorSeleccionado.setApellidos(vista.txtApellidosTrabajador.getText());
                trabajadorSeleccionado.setFechaNacimiento(Date.valueOf(vista.dateFechaNacTrabajador.getDate()));
                trabajadorSeleccionado.setCargo(String.valueOf(vista.cbCargoTrabajador.getSelectedItem()));
                trabajadorSeleccionado.setTienda((Tienda)vista.cbTiendaTrabajador.getSelectedItem());
                modelo.modificarTrabajador(trabajadorSeleccionado);
                refrescarTrabajador();
                listarTrabajadores(modelo.getTrabajador());
                break;
            /**
             * Case que elimina un trabajador
             */
            case "EliminarTrabajador":
                Trabajador trabajadorEliminado = (Trabajador)vista.listaTrabajadores.getSelectedValue();
                modelo.eliminarTrabajador(trabajadorEliminado);
                listarTrabajadores(modelo.getTrabajador());
                break;
            /**
             * Case que lista un trabajador
             */
            case "ListarTrabajador":
                listarTrabajadores(modelo.getTrabajador());
                break;
        }
    }

    /**
     * Metodo que refresca los cambos de una prenda
     */
    private void refrescarPrenda() {
        vista.txtNombrePrenda.setText("");
        vista.cbTipoPrenda.setSelectedItem(-1);
        vista.cbColorPrenda.setSelectedItem(-1);
        vista.sliderTallaPrenda.setValue(34);
        vista.txtPrecioPrenda.setText("");
        vista.dateFechaCompraPrenda.setText("");
        vista.dateFechaEntregaPrenda.setText("");
        vista.cbColorPrenda.setSelectedItem(-1);
        vista.cbColeccionPrenda.setSelectedItem(-1);
    }
    /**
     * Metodo que refresca los cambos de una tienda
     */
    private void refrescarTienda() {
        vista.txtNombreTienda.setText("");
        vista.txtDireccionTienda.setText("");
        vista.sliderPlantasTienda.setValue(3);
    }
    /**
     * Metodo que refresca los cambos de una coleccion
     */
    private void refrescarColeccion() {
        vista.txtNombreColeccion.setText("");
        vista.cbMarcaColeccion.setSelectedItem(-1);
        vista.cbEstacionColeccion.setSelectedItem(-1);
        vista.dateFechaCreacionColeccion.setText("");
        vista.cbDiseniadorColeccion.setSelectedItem(-1);
    }
    /**
     * Metodo que refresca los cambos de un trabajador
     */
    private void refrescarTrabajador() {
        vista.txtNombreTrabajador.setText("");
        vista.txtApellidosTrabajador.setText("");
        vista.dateFechaNacTrabajador.setText("");
        vista.cbCargoTrabajador.setSelectedItem(-1);
        vista.cbTiendaTrabajador.setSelectedItem(-1);
    }
    /**
     * Metodo que refresca los cambos de un diseñador
     */
    private void refrescarDiseniador() {
        vista.txtNombreDisen.setText("");
        vista.txtApellidosDisen.setText("");
        vista.dateFechaNacDisen.setText("");
        vista.cbGeneroDisen.setSelectedItem(-1);
    }

    /**
     * Metodo que lista los diseñadores y metodo que añade los diseñadores al combobox de diseñadores de coleccion
     * @param diseniadores
     */
    private void listarDiseniadores(ArrayList<Diseniador> diseniadores){
        vista.dlmDiseniador.clear();
        for (Diseniador diseniador : diseniadores){
            vista.dlmDiseniador.addElement(diseniador);
        }
        ArrayList<Diseniador> dis = modelo.getDiseniador();
        for(Diseniador diseniador : dis){
            vista.cbDiseniadorColeccion.addItem(diseniador);
        }
    }

    /**
     * Metodo que lista las colecciones y añade las colecciones al combo box de coleccion de prenda
     * @param colecciones
     */
    private void listarColecciones(ArrayList<Coleccion> colecciones){
        vista.dlmColeccion.clear();
        for (Coleccion coleccion : colecciones){
            vista.dlmColeccion.addElement(coleccion);
        }
        ArrayList<Coleccion> col = modelo.getColeccion();
        for(Coleccion coleccion : col){
            vista.cbColeccionPrenda.addItem(coleccion);
        }
    }

    /**
     * Metodo que lista las prendas
     * @param prendas
     */
    private void listarPrendas(ArrayList<Prenda> prendas){
        vista.dlmPrenda.clear();
        for (Prenda prenda : prendas){
            vista.dlmPrenda.addElement(prenda);
        }
    }

    /**
     * Metodo que lista las tiendas y añade las tiendas al combobox de tiendas de trabajador
     * @param tiendas
     */
    private void listarTiendas(ArrayList<Tienda> tiendas){
        vista.dlmTienda.clear();
        for (Tienda tienda : tiendas){
            vista.dlmTienda.addElement(tienda);
        }
        ArrayList<Tienda> tiendas1 = modelo.getTienda();
        for(Tienda tienda : tiendas1){
            vista.cbTiendaTrabajador.addItem(tienda);
        }
    }

    /**
     * Metodo que lista los trabajadores
     * @param trabajadores
     */
    private void listarTrabajadores(ArrayList<Trabajador> trabajadores){
        vista.dlmTrabajador.clear();
        for (Trabajador trabajador : trabajadores){
            vista.dlmTrabajador.addElement(trabajador);
        }
    }

    /**
     * Metodo que lista las colecciones de un diseñador
     * @param coleccionesDiseniador
     */
    private void listarColeccionesDiseniador(ArrayList<Coleccion> coleccionesDiseniador){
        vista.dlmColeccionDisen.clear();
        for (Coleccion col : coleccionesDiseniador){
            vista.dlmColeccionDisen.addElement(col);
        }
    }

    /**
     * Metodo que lista las prendas de una coleccion
     * @param prendaColeccion
     */
    private void listarPrendasColeccion(ArrayList<Prenda> prendaColeccion){
        vista.dlmPrendaColeccion.clear();
        for (Prenda pre : prendaColeccion){
            vista.dlmPrendaColeccion.addElement(pre);
        }
    }

    /**
     * Metodo que lista los trabajadores de una tienda
     * @param trabajadorTienda
     */
    private void listarTrabajadorTienda(ArrayList<Trabajador> trabajadorTienda){
        vista.dlmTrabajadorTienda.clear();
        for (Trabajador tra : trabajadorTienda){
            vista.dlmTrabajadorTienda.addElement(tra);
        }
    }

    /**
     * Metodo value changed que al seleccionar un campo de una lista pinta los campos en sus respectivos huecos para así editarlos con facilidad
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listaDiseniadores){
                Diseniador seleccionDisen = (Diseniador) vista.listaDiseniadores.getSelectedValue();
                vista.txtNombreDisen.setText(seleccionDisen.getNombre());
                vista.txtApellidosDisen.setText(seleccionDisen.getApellidos());
                vista.dateFechaNacDisen.setText(String.valueOf(seleccionDisen.getFechaNacimiento()));
                vista.cbGeneroDisen.setSelectedItem(seleccionDisen.getGenero());
            }else if(e.getSource() == vista.listaColecciones){
                Coleccion seleccionColeccion = (Coleccion) vista.listaColecciones.getSelectedValue();
                vista.txtNombreColeccion.setText(seleccionColeccion.getNombre());
                vista.cbMarcaColeccion.setSelectedItem(seleccionColeccion.getMarca());
                vista.cbEstacionColeccion.setSelectedItem(seleccionColeccion.getEstacion());
                vista.dateFechaCreacionColeccion.setText(String.valueOf(seleccionColeccion.getFechaCreacion()));
                vista.cbDiseniadorColeccion.setSelectedItem(seleccionColeccion.getDiseniador());
            }else if(e.getSource() == vista.listaPrendas){
                Prenda seleccionPrenda = (Prenda)vista.listaPrendas.getSelectedValue();
                vista.txtNombrePrenda.setText(seleccionPrenda.getNombre());
                vista.cbTipoPrenda.setSelectedItem(seleccionPrenda.getTipo());
                vista.cbColorPrenda.setSelectedItem(seleccionPrenda.getColor());
                vista.sliderTallaPrenda.setValue(seleccionPrenda.getTalla());
                vista.txtPrecioPrenda.setText(String.valueOf(seleccionPrenda.getPrecio()));
                vista.dateFechaCompraPrenda.setText(String.valueOf(seleccionPrenda.getFechaCompra()));
                vista.dateFechaEntregaPrenda.setText(String.valueOf(seleccionPrenda.getFechaEntrega()));
                vista.cbColeccionPrenda.setSelectedItem(seleccionPrenda.getColeccion());
            }else if(e.getSource() == vista.listaTiendas){
                Tienda seleccionTienda = (Tienda)vista.listaTiendas.getSelectedValue();
                vista.txtNombreTienda.setText(seleccionTienda.getNombre());
                vista.txtDireccionTienda.setText(seleccionTienda.getDireccion());
                vista.sliderPlantasTienda.setValue(seleccionTienda.getPlantas());
            }else if (e.getSource() == vista.listaTrabajadores){
                Trabajador seleccionTrabajador = (Trabajador)vista.listaTrabajadores.getSelectedValue();
                vista.txtNombreTrabajador.setText(seleccionTrabajador.getNombre());
                vista.txtApellidosTrabajador.setText(seleccionTrabajador.getApellidos());
                vista.dateFechaNacTrabajador.setText(String.valueOf(seleccionTrabajador.getFechaNacimiento()));
                vista.cbCargoTrabajador.setSelectedItem(seleccionTrabajador.getCargo());
                vista.cbTiendaTrabajador.setSelectedItem(seleccionTrabajador.getTienda());
            }
        }
    }
}
