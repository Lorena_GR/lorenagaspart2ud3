package com.lorenagr.ropa_ceremonia.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Tienda {
    private int id;
    private String nombre;
    private String direccion;
    private int plantas;
    private List<Trabajador> trabajadores;
    private List<Coleccion> colecciones;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "plantas")
    public int getPlantas() {
        return plantas;
    }

    public void setPlantas(int plantas) {
        this.plantas = plantas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return id == tienda.id &&
                plantas == tienda.plantas &&
                Objects.equals(nombre, tienda.nombre) &&
                Objects.equals(direccion, tienda.direccion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, direccion, plantas);
    }

    @OneToMany(mappedBy = "tienda")
    public List<Trabajador> getTrabajadores() {
        return trabajadores;
    }

    public void setTrabajadores(List<Trabajador> trabajadores) {
        this.trabajadores = trabajadores;
    }

    @ManyToMany(mappedBy = "tiendas")
    public List<Coleccion> getColecciones() {
        return colecciones;
    }

    public void setColecciones(List<Coleccion> colecciones) {
        this.colecciones = colecciones;
    }

    public Tienda(String nombre, String direccion, int plantas) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.plantas = plantas;
        this.trabajadores = new ArrayList<>();
        this.colecciones = new ArrayList<>();
    }

    public Tienda(){

    }

    @Override
    public String toString() {
        return "Tienda{" +
                "nombre='" + nombre + '\'' +
                ", direccion='" + direccion + '\'' +
                ", plantas=" + plantas +
                '}';
    }

}
