package com.lorenagr.ropa_ceremonia.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Coleccion {
    private int id;
    private String nombre;
    private String marca;
    private String estacion;
    private Date fechaCreacion;
    private Diseniador diseniador;
    private List<Prenda> prendas;
    private List<Tienda> tiendas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "estacion")
    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coleccion coleccion = (Coleccion) o;
        return id == coleccion.id &&
                Objects.equals(nombre, coleccion.nombre) &&
                Objects.equals(marca, coleccion.marca) &&
                Objects.equals(estacion, coleccion.estacion) &&
                Objects.equals(fechaCreacion, coleccion.fechaCreacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, marca, estacion, fechaCreacion);
    }

    @ManyToOne
    @JoinColumn(name = "id_diseniador", referencedColumnName = "id")
    public Diseniador getDiseniador() {
        return diseniador;
    }

    public void setDiseniador(Diseniador diseniador) {
        this.diseniador = diseniador;
    }

    @OneToMany(mappedBy = "coleccion")
    public List<Prenda> getPrendas() {
        return prendas;
    }

    public void setPrendas(List<Prenda> prendas) {
        this.prendas = prendas;
    }

    @ManyToMany
    @JoinTable(name = "coleccion_tienda", catalog = "", schema = "ropa_ceremonia_ud3", joinColumns = @JoinColumn(name = "idtienda", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "idcoleccion", referencedColumnName = "id", nullable = false))
    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }

    public Coleccion(String nombre, String marca, String estacion, Date fechaCreacion, Diseniador diseniador) {
        this.nombre = nombre;
        this.marca = marca;
        this.estacion = estacion;
        this.fechaCreacion = fechaCreacion;
        this.diseniador = diseniador;
        this.prendas = new ArrayList<>();
        this.tiendas = new ArrayList<>();
    }

    public Coleccion(){

    }

    @Override
    public String toString() {
        return "Coleccion{" +
                "nombre='" + nombre + '\'' +
                ", marca='" + marca + '\'' +
                ", estacion='" + estacion + '\'' +
                ", fechaCreacion=" + fechaCreacion +
                ", diseniador=" + diseniador +
                '}';
    }

}
